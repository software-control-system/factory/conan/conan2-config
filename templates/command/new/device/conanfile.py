{%- set package = package | default('MyDevice') -%}
{%- set name = name | default('mydevice') -%}
{%- set version = version | default(1.0.0) -%}
{%- set executable = executable | default('ds_{}'.format(name)) -%}
{%- set license = license | default('<Put the package license here>') -%}
{%- set author = author | default('<Put your name here> <And your email here>') -%}
{%- set url = url | default('<Package recipe repository url here, for issues about the package>') -%}
{%- set description = description | default('<Description of {} package here>'.format(name)) -%}
{%- set requires = requires | default(['yat4tango/[>=1.0]@soleil/stable']) -%}
from conan import ConanFile
{% if git is defined -%}from conan.tools.scm import Git{% endif %}

class {{ package }}Recipe(ConanFile):
    name = "{{ name }}"
    executable = "{{ executable }}"
    version = "{{ version }}"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "{{ license }}"
    author = "{{ author }}"
    url = "{{ url }}"
    description = "{{ description }}"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt"{%- if git is not defined %}, "src/*"{% endif %}
    
    {% if git is defined -%}
    def source(self):
        git = Git(self, "target")
        git.clone(url="{{git}}", target=".")
        {%- if commit is defined %}
        git.checkout(commit="{{commit}}")
        {% endif %}
    {% endif -%}

    def requirements(self):
        {%- if requires is defined %}
        {%- for require in requires %}
        self.requires("{{ require }}")
        {% endfor -%}
        {% endif -%}
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
