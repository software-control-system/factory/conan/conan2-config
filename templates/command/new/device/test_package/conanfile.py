{%- set package = package | default('MyDevice') -%}
{%- set name = name | default('mydevice') -%}
{%- set executable = executable | default('ds_{}'.format(name)) -%}
import os
from conan import ConanFile
from conan.tools.build import can_run


class {{package}}TestConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def test(self):
        if can_run(self):
            if self.settings.os == "Windows":
                self.run("{{executable}} 2>&1 | findstr \"usage\"", env="conanrun")
            else:
                self.run("{{executable}} 2>&1 | grep \"usage\"", env="conanrun")
