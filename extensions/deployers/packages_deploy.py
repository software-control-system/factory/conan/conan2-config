import os

from conan.errors import ConanException
from conan.tools.files import copy
from conan.api.output import ConanOutput

def deploy(graph, output_folder, **kwargs):
    conanfile = graph.root.conanfile
    conanfile.output.info(f"Conan Soleil deployer to install package into {output_folder} folder")
    for name, dep in graph.root.conanfile.dependencies.items():
        conanfile.output.info(f"Install {dep}")
        if dep.package_folder is None:
            continue
        _deploy_single(dep, conanfile, output_folder)

def _deploy_single(dep, conanfile, output_folder):
    if conanfile.settings.os == "Windows":
        symlinks=False
    else:
        symlinks=True
    try:
        copy(conanfile, "ds_*", os.path.join(dep.package_folder,"bin"), output_folder)
        copy(conanfile, "*.dll", os.path.join(dep.package_folder,"bin"), output_folder)
        copy(conanfile, "*.so*", os.path.join(dep.package_folder,"lib"), os.path.join(output_folder,"lib"))
    except Exception as e:
        raise ConanException(f"packages_deploy: Install of '{dep}' files failed: {e}.")
        
    dep.set_deploy_folder(output_folder)

        
